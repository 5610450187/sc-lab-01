package Control;

import View.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import Model.Card;

public class Main {
	ActionListener list;
	Frame frame;
	double total;

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main();
	}

	public Main() {
		frame = new Frame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
		
	}

	public void setTestCase() {
		Card card = new Card("meng","5610450381",500); 
		
		frame.setResult(card.getName()+"  " +card.getId()+"  "+"\n"+"Total: "+card.getMoney()+"\n"
						+"Buy Item 150 : Balance "+buyItem(card, 150)+"\n"
						+"Total is " + checkMoney(card)
						+ "\n" + "Deposit 50 : Balance "+addMoney(card, 50));
		
		;
		

	}
	public double buyItem (Card card,double cost){
		total = card.getMoney();
		total -= cost;
		card.setMoney(total);
		return total ;
		
	}
	public double checkMoney(Card card){
		return card.getMoney();
		
	}
	public double addMoney (Card card,double money){
		total = card.getMoney();
		total += money;
		card.setMoney(total);
		return total ;
		
	}

	
	
}
